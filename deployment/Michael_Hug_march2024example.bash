#!/bin/bash

CLUSTER_NAME='michael-hug'

eksctl create cluster --name $CLUSTER_NAME

# Use with curl -O https://raw.githubusercontent.com/kubernetes-sigs/aws-load-balancer-controller/v2.7.1/docs/install/iam_policy.json
#aws iam create-policy --policy-name AWSLoadBalancerControllerIAMPolicy --policy-document file://iam_policy.json
eksctl create iamserviceaccount \
  --cluster=$CLUSTER_NAME \
  --namespace=kube-system \
  --name=aws-load-balancer-controller \
  --role-name AmazonEKSLoadBalancerControllerRole \
  --attach-policy-arn=arn:aws:iam::243743871854:policy/AWSLoadBalancerControllerIAMPolicy \
  --approve

# Push to ECR
#aws ecr-public get-login-password --region us-east-1 | docker login --username AWS --password-stdin public.ecr.aws/b6n9v2v9
#docker build -t michael_hug_march2024example .
#docker tag michael_hug_march2024example:latest public.ecr.aws/b6n9v2v9/michael_hug_march2024example:latest
#docker push public.ecr.aws/b6n9v2v9/michael_hug_march2024example:latest

eksctl utils associate-iam-oidc-provider --cluster $CLUSTER_NAME --approve
helm repo add eks https://aws.github.io/eks-charts
helm install aws-load-balancer-controller eks/aws-load-balancer-controller -n kube-system --set clusterName=$CLUSTER_NAME --set serviceAccount.create=false --set serviceAccount.name=aws-load-balancer-controller
helm repo update eks


kubectl apply -f Michael_Hug_march2024example.yaml

#kubectl delete namespace michaelhug-march2024example
#eksctl delete cluster --name $CLUSTER_NAME
#echo "do the following at the end : #eksctl delete cluster --name $CLUSTER_NAME"
