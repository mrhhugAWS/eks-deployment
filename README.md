# eks deployment



## Getting started


You will need a collection of libs installed on your local machine for this to work, but to deploy the cluster and app see the deployment/Michael_Hug_march2024example.bash script. The Dockerfile is named such.

I did a few things manually, like setup an AWS role, configured my local client, pushed to ecr, setup 443 on the security group and LB, but the bulk of the deployment can be completed with the script mentioned above.
